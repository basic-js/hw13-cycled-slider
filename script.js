const stopBtn = document.querySelector('#stop-btn');
const startBtn = document.querySelector('#start-btn');

const images = document.querySelectorAll('.image-to-show');
const firstImg = images[0];
const lastImg = images[images.length - 1];

const changeImg = () => {
    const currentImg = document.querySelector('.active');
    if(currentImg !== lastImg){
        currentImg.classList.remove('active');
        currentImg.nextElementSibling.classList.add('active'); 
    } else {
        currentImg.classList.remove('active');
        firstImg.classList.add('active'); 
    }
}

let timer = setInterval(changeImg, 3000);

stopBtn.addEventListener('click', () => {
    clearInterval(timer);
    startBtn.disabled = false;
    stopBtn.disabled = true;
  
})

startBtn.addEventListener('click', () => {
    timer = setInterval(changeImg, 3000);
    startBtn.disabled = true;
    stopBtn.disabled = false; 
})
